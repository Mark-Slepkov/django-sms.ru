#!/bin/bash
virtualenv --python=/usr/bin/python3 ./venv && \
./venv/bin/pip install . --upgrade &&\
source ./venv/bin/activate