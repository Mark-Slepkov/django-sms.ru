#!/bin/bash
# /venv/bin/pip3 install . --process-dependency-links --upgrade
/venv/bin/python3 ./manage.py makemigrations
/venv/bin/python3 ./manage.py migrate
# ./venv/bin/python3 ./manage.py collectstatic
# ./venv/bin/python3 ./manage.py initadmin

# ./venv/bin/python3 ./manage.py startapp product

/venv/bin/python3 ./manage.py runserver 0.0.0.0:8000
