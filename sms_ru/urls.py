from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'sms_ru/^$', views.sms_ru_view.as_view(), name='sms_rus'),
]