from django.test import TestCase
from .apps import SMSRu, SMSRuConfig, ErrorSending
import json


class TestSnippets(TestCase):
    api_id = ''
    login = ''
    password = ""
    necorrectpass = ""
    to_phone = ""

    def setUp(self):
        self.conf = SMSRuConfig(
            api_id=self.api_id,
            login=self.login,
            password=self.password,
            test=True,
            sms_from="gf",
            translit=False,
            partner_id="fd"
        )
        self.conf.save()
        self.conn = SMSRu()
        pass

    def test_sms_ru_1(self):
        data = SMSRuConfig.get_solo()
        self.assertEqual(data.api_id, self.api_id)

    def test_sms_ru_2(self):
        ddata = json.loads(self.conn.send_single_message(self.to_phone, 'test'))
        print("Checking authorization and sending sms, via api_id")
        # print(ddata)
        self.assertEqual(ddata["status"], "OK")
        print(" OK")

    def test_sms_ru_3(self):
        self.conn.api_id = ''
        print("Verification of authorization and sending SMS, via login password, in the absence of api_id")
        ddata = json.loads(self.conn.send_single_message(self.to_phone, 'test'))
        # print(ddata)
        self.assertEqual(ddata["status"], "OK")
        print(" OK")

    def test_sms_ru_4(self):
        self.conn.api_id = ''
        self.conn.password = self.necorrectpass
        print("Check for a reaction if there is no api_id and the wrong password")
        try:
            ddata = json.loads(self.conn.send_single_message(self.to_phone, 'test'))
            # print(ddata)
            self.assertEqual(ddata['status'], 'ERROR')
        except ErrorSending:
            self.assertEqual(1, 1)
            # print("ERROR впоймался ожидаемый: ")
        print(" OK")
