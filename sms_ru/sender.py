from .models import SMSRuConfig
import requests
import json


class ErrorSending(Exception):
    pass


class SMSRu(object):
    def __init__(self):
        obj = SMSRuConfig.get_solo()
        self.api_id = obj.api_id
        self.partner_id = obj.partner_id
        self.translit = "1" if obj.translit else "0"
        self.password = obj.password
        self.login = obj.login
        self.test = "1" if obj.test else "0"
        self.sms_from = obj.sms_from
        self.con = requests.session()

    def send_single_message(self, phone, msg):
        url = 'https://sms.ru/sms/send?'
        data = {
            "json": 1,
            "to": phone,
            "msg": msg,
            "partner_id": self.partner_id,
            "translit": self.translit,
            "test": self.test,
            "from": self.sms_from
        }
        if (len(self.api_id) > 0):
            data["api_id"] = self.api_id
        else:
            data["login"] = self.login
            data["password"] = self.password
        req = self.con.get(url, params=data)
        _json = json.loads(req.text)
        if (req.status_code != 200 or _json['status'] == 'ERROR'):
            raise ErrorSending(req.text)
        # print(req.url)
        return req.text
