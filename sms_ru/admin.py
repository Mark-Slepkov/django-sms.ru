from django.contrib import admin
from sms_ru.models import SMSRuConfig
from solo.admin import SingletonModelAdmin

admin.site.register(SMSRuConfig,SingletonModelAdmin)