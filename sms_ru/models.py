from django.db import models
from solo.models import SingletonModel

# Create your models here.

class SMSRuConfig(SingletonModel):
    #http://sms.ru/api/send
    api_id = models.CharField(max_length=64,verbose_name="Секретный ключ на сервисе sms.ru",blank=True, default="")
    sms_from = models.CharField(max_length=64,verbose_name="Имя отправителя",blank=True, default="")
    translit = models.BooleanField(verbose_name="Переводить все русские символы в латинские",blank=True,default=False)
    test = models.BooleanField(verbose_name="Режим тестирования",blank=True,default=True)
    partner_id = models.CharField(max_length=64,verbose_name="ид в партнерской программе",blank=True, default="")
    login = models.CharField(max_length=64,verbose_name="Логин")
    password = models.CharField( max_length=64, verbose_name="Пароль")
    class Meta:
        verbose_name = "SMSRu Config"

    def save(self, *args, **kwargs):
        return super().save(*args, **kwargs)
